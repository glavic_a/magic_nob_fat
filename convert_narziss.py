"""
Script to convert two NARZISS polarized reflectivity files into
ORSO .ort file format.
"""

import os
import sys

from dataclasses import dataclass
from datetime import datetime

import yaml

from numpy import array, loadtxt, ndarray, pi, sin, sqrt
from orsopy import fileio

__version__ = "1.1"

config = yaml.safe_load(open("convert_narziss.yaml", "r"))


@dataclass
class BeamlineConfig:
    I0_UP: float
    I0_DOWN: float
    P_UP: float
    P_DOWN: float
    S1_DISTANCE: float
    S1_WIDTH: float
    S2_DISTANCE: float
    S2_WIDTH: float


@dataclass
class SampleConfig:
    WIDTH: float
    HEIGH: float
    THICKNESS: float


bl_config = BeamlineConfig(**config["beamline"])
smpl_config = SampleConfig(**config["sample"])


@dataclass
class NARZISSdata:
    file_name: str
    header: dict[str, str]

    omega_min: float
    omega_max: float
    lamda: float

    Q: ndarray
    I: ndarray
    dI: ndarray


def read_narzissfile(fname: str) -> NARZISSdata:
    with open(fname, "r") as f:
        if "NARZISS" not in f.readline():
            raise IOError("Wrong file format, expecting NARZISS ASCII file")
        header = ""
        while True:
            nline = f.readline()
            if nline.startswith("NP"):
                columns = nline.split()
                break
            header += nline
        header_data = {}
        for line in header.splitlines():
            if "=" in line:
                name, value = line.split("=", 1)
                header_data[name.strip()] = value.strip()

        data = loadtxt(f, comments="END-OF-DATA").T

    omega = data[columns.index("som")]
    lamda = float(header_data["Lambda"])
    Q = 4 * pi / lamda * sin(omega * pi / 180.0)
    counts = data[columns.index("COUNTS")]
    monitor = data[columns.index("MONITOR1")]
    I = counts / monitor
    dI = sqrt(counts) / monitor
    return NARZISSdata(os.path.basename(fname), header_data, omega.min(), omega.max(), lamda, Q, I, dI)


def create_orso(su: NARZISSdata, sd: NARZISSdata) -> tuple[fileio.OrsoDataset, fileio.OrsoDataset]:
    sample = fileio.Sample(
        name=su.header["Sample Name"].split()[0],
        size=fileio.ValueVector(x=smpl_config.WIDTH, y=smpl_config.HEIGH, z=smpl_config.THICKNESS, unit="cm"),
    )
    owner = fileio.Person(
        name=su.header["User"], affiliation="Paul Scherrer Institut", contact="christine.klauser@psi.ch"
    )

    experiment = fileio.Experiment(
        title=su.header["Title"],
        instrument="NARZISS",
        start_date=datetime.fromisoformat(su.header["File Creation Date"]),
        probe="neutron",
        facility="SINQ",
        proposalID=su.header["ProposalID"],
    )

    measurement = fileio.Measurement(
        instrument_settings=fileio.InstrumentSettings(
            incident_angle=fileio.ValueRange(min=su.omega_min, max=su.omega_max, unit="degree"),
            wavelength=fileio.Value(magnitude=su.lamda, unit="Angstrom"),
            polarization=fileio.Polarization.po,
        ),
        data_files=[su.file_name],
        scheme="angle-dispersive",
    )
    if "coll=" in su.header["Title"]:
        s1s, s2s = su.header["Title"].split("coll=")[1].split(",")
        S1w = float(s1s.split("/")[0].strip())
        S2w = float(s2s.split("/")[0].strip())
    else:
        S1w = bl_config.S1_WIDTH
        S2w = bl_config.S2_WIDTH
    # add user parameters to describe beam settings
    measurement.instrument_settings.slit_configuration = {
        "divergence_distance": fileio.Value(bl_config.S1_DISTANCE, "mm"),
        "divergence_opening": fileio.Value(S1w, "mm"),
        "size_distance": fileio.Value(bl_config.S2_DISTANCE, "mm"),
        "size_opening": fileio.Value(S2w, "mm"),
    }
    measurement.instrument_settings.incident_intensity = fileio.Value(bl_config.I0_UP, "1/monitor")
    measurement.instrument_settings.incident_polarization = bl_config.P_UP

    data_source = fileio.DataSource(owner, experiment, sample, measurement)

    reduction = fileio.Reduction(
        software=fileio.Software(name="convert_narziss.py", version=__version__, platform=sys.platform),
        corrections=["Calculate Q from Angle"],
    )

    columns = [
        fileio.Column(name="Q", unit="1/Angstrom"),
        fileio.Column(name="I", unit="1/monitor"),
        fileio.ErrorColumn(error_of="I"),
    ]

    osu_hdr = fileio.Orso(data_source, reduction, columns, "spin-up")
    # copy and modify header parts that changed from spin-up to spin-down
    data_source = fileio.DataSource.from_dict(data_source.to_dict())
    data_source.measurement.instrument_settings.polarization = fileio.Polarization.mo
    data_source.measurement.data_files = [sd.file_name]
    data_source.measurement.instrument_settings.incident_intensity = fileio.Value(bl_config.I0_DOWN, "1/monitor")
    data_source.measurement.instrument_settings.incident_polarization = bl_config.P_DOWN
    osd_hdr = fileio.Orso(data_source, reduction, columns, "spin-down")

    return (
        fileio.OrsoDataset(osu_hdr, array([su.Q, su.I, su.dI]).T),
        fileio.OrsoDataset(osd_hdr, array([sd.Q, sd.I, sd.dI]).T),
    )


def main():
    if len(sys.argv) != 3:
        raise RuntimeError("You have to provide one spin-up and one spin-down file for conversion")
    su = read_narzissfile(sys.argv[1])
    sd = read_narzissfile(sys.argv[2])
    osu, osd = create_orso(su, sd)
    fileio.save_orso([osu, osd], f"NOB_reflectivity_{osu.info.data_source.sample.name}.ort")


if __name__ == "__main__":
    main()
