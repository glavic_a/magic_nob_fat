"""
Program to analyze polarized neutron reflectivity measurements
for the MAGIC analyzer project with Nano Optics Berlin.
"""

import os.path
import sys

from dataclasses import dataclass
from tkinter import LEFT, IntVar, Label, Radiobutton, Tk, W, filedialog
from typing import Optional

import yaml

from matplotlib import pyplot as plt
from numpy import (arcsin, arctan, array, cos, exp, nan, nanmean, nanmin, ndarray, pi, sin, sqrt, trapz, where,
                   zeros_like)
from numpy.linalg import inv
from orsopy import fileio
from scipy.optimize import curve_fit

config = yaml.safe_load(open("magic_reflectivity_report.yaml", "r"))


MM2INCHES = 1.0 / 25.4
Q2M = 1 / 0.0218
FIT_ALPHA = config["FIT_ALPHA"]

REPORT_REVISION = "0.6"
INTERFACE_GUI = True

L = 12.0  # default sample length - cm
D = 0.5  # default sample thickness - cm

BAD_RESULT_OPTIONS = ["To PSI for Evaluation", "Accepted by PSI", "Rejected by PSI"]


class CONFIG:
    ### constants from specification
    M_ref = 3.0  # location for reference reflectivity at high m
    R_ref = 0.8  # reflectivity at reference location
    M_max = 3.2  # m-value from specification (theoretical curve drop)
    Alpha_spec = 0.075  # alpha for theoretical curve
    alpha_max = 0.075  # limit for measured alpha from specification
    R_div_max = 0.1  # maximum deviation from theoretical curve
    P_min = 0.95  # minimum polarization over Q-range

    ### constants used for analysis ###
    # MU = 0.0085329/1.798197 # specific silicon absorption coefficient at 1 Å wavelength - 1/cm/Å
    MU = 0.0314 / 5.0  # measured silicon absorption coefficient at 1 Å wavelength - 1/cm/Å
    Q_Pstart = config["Q_Pstart"]
    Q_Pend = M_max / Q2M

    E_LABEL = {True: "in specification", False: "out of specification"}


def read_data(file_name) -> tuple[fileio.OrsoDataset, fileio.OrsoDataset]:
    """
    Load the spin-up/spin-down data from ORSO file and check that it is reasonable for this analysis.
    """
    res1, res2 = fileio.load_orso(file_name)
    if (
        res1.info.data_source.measurement.instrument_settings.polarization != fileio.Polarization.po
        or res2.info.data_source.measurement.instrument_settings.polarization != fileio.Polarization.mo
    ):
        raise ValueError("The file should contain first one spin-up and then one spin-down datasets.")
    return res1, res2


@dataclass
class ResultParameters:
    """
    Collects all relevant analysis results for the report.
    """

    run_id: str

    alpha: float
    scale: float

    beam_size: float
    sample_length: float
    sample_thickness: float

    R_m_ref: float
    R_div_max: float
    Pmin: float
    Pavg: float
    Pup: float
    Pdown: float

    ref_in_spec: bool
    pol_in_spec: bool
    evaluation: Optional[str]


@dataclass
class MeasurementMeta:
    instrument: str
    start_time: str
    owner: str


@dataclass
class PlotData:
    """
    Collects data points for plots in the report.
    """

    Q: ndarray

    Iup: ndarray
    dIup: ndarray
    Idown: ndarray
    dIdown: ndarray

    cabs: ndarray
    cfp: ndarray

    Rup: ndarray
    Rdown: ndarray
    P: ndarray
    Pm: ndarray

    axes_labels: dict[str, str]
    meta: MeasurementMeta


class DataAnalyzer:
    beam_size = 0.1  # cm

    # reference intensities to be used
    I0up: float
    I0down: float

    Q: ndarray

    def __init__(self, sup: fileio.OrsoDataset, sdown: fileio.OrsoDataset):
        self.sup = sup
        self.sdown = sdown

        self.calculate_corrections()

    def calculate_corrections(self):
        self.I0up = self.sup.info.data_source.measurement.instrument_settings.user_data["incident_intensity"][
            "magnitude"
        ]
        self.I0down = self.sdown.info.data_source.measurement.instrument_settings.user_data["incident_intensity"][
            "magnitude"
        ]

        self.set_intensities()
        self.set_sample_geometry()
        self.set_beam_geometry()

        self.set_absorption_correction()
        self.set_footprint_correction()
        self.set_polarization_correction()

        self.analyze_polarization()
        self.analyze_reflectivity()

    def set_intensities(self):
        Iup = self.sup.data[:, 1]
        dIup = self.sup.data[:, 2]
        Idown = self.sdown.data[:, 1]
        dIdown = self.sdown.data[:, 2]
        if Iup.shape[0] >= Idown.shape[0]:
            self.Q = self.sup.data[:, 0]
            Idown, dIdown = self.extend_short_dataset(Idown, Iup, dIdown)
        else:
            self.Q = self.sdown.data[:, 0]
            Idown, dIdown = self.extend_short_dataset(Iup, Idown, dIup)
        self.Iup = Iup
        self.Idown = Idown
        self.dIup = dIup
        self.dIdown = dIdown

    @staticmethod
    def extend_short_dataset(Ishort, Ilong, dIshort):
        """
        If one dataset is shorter than the other, extend to same number of points filling up with NaN.
        """
        Iout = zeros_like(Ilong)
        dIout = zeros_like(Ilong)
        Iout[: Ishort.shape[0]] = Ishort
        Iout[Ishort.shape[0] :] = nan
        dIout[: Ishort.shape[0]] = dIshort
        dIout[Ishort.shape[0] :] = nan
        return Iout, dIout

    def analyze_polarization(self):
        # perform polarization correction by multiplication with inverse polarization matrix
        Rup = self.Pm[0, 0] * self.Iup + self.Pm[0, 1] * self.Idown
        Rdown = self.Pm[1, 0] * self.Iup + self.Pm[1, 1] * self.Idown
        dRup = sqrt((self.Pm[0, 0] * self.dIup) ** 2 + (self.Pm[1, 0] * self.dIdown) ** 2)
        dRdown = sqrt((self.Pm[1, 0] * self.dIup) ** 2 + (self.Pm[1, 1] * self.dIdown) ** 2)
        self.Rup = Rup
        self.dRup = dRup
        self.Rdown = Rdown
        self.dRdown = dRdown
        self.P = (self.Rup - self.Rdown) / (self.Rup + self.Rdown)
        self.Pmin = float(nanmin(self.P[(self.Q >= CONFIG.Q_Pstart) & (self.Q <= CONFIG.Q_Pend)]))
        self.Pavg = float(nanmean(self.P[(self.Q >= CONFIG.Q_Pstart) & (self.Q <= CONFIG.Q_Pend)]))

    def analyze_reflectivity(self):
        # calculate reflectivity FoM:
        R = self.Rup / self.cabs / self.cfp
        if FIT_ALPHA:
            fit_range = (self.Q > CONFIG.Q_Pstart) & (self.Q <= ((CONFIG.M_max + 0.05) / Q2M))
            p0 = [1.0, CONFIG.Alpha_spec]
            m = self.Q[fit_range] * Q2M
            res = curve_fit(self.ideal_reflectivity, xdata=m, ydata=R[fit_range], p0=p0, method="lm", full_output=True)
            self.scale, self.alpha = res[0]
        else:
            c_range = (self.Q > (0.5 / Q2M)) & (self.Q <= ((1.0) / Q2M))
            a_range = (self.Q > (1.0 / Q2M)) & (self.Q <= ((CONFIG.M_max + 0.05) / Q2M))
            C = R[c_range].mean()
            self.alpha = 2.0 / (CONFIG.M_max - 1) - 2 * trapz(R[a_range], self.Q[a_range] * Q2M) / (
                C * (CONFIG.M_max - 1) ** 2
            )
            self.scale = C
        fltr = (self.Q > CONFIG.Q_Pstart) & (self.Q <= CONFIG.Q_Pend)
        ref = self.ideal_reflectivity(self.Q * Q2M, 1.0, CONFIG.Alpha_spec)
        self.R_div_max_exp = (1.0 - R[fltr] / ref[fltr]).max()
        self.R_m_ref = float(R[(self.Q > (CONFIG.M_ref / Q2M))][0])

    @staticmethod
    def ideal_reflectivity(m, scale, alpha):
        m_max = CONFIG.M_max
        out = scale * where(m < 1.0, 1.0, where(m <= m_max, 1.0 - (m - 1.0) * alpha, 0.0))
        return out

    def set_sample_geometry(self):
        mdata: fileio.Orso = self.sup.info
        sample_size = getattr(mdata.data_source.sample, "size", None)
        if sample_size is None:
            self.sample_length = L
            self.sample_thickness = D
        else:
            self.sample_length = sample_size.as_unit("cm")[0]
            self.sample_thickness = sample_size.as_unit("cm")[2]

    def set_beam_geometry(self):
        mdata: fileio.Orso = self.sup.info
        slit = mdata.data_source.measurement.instrument_settings.user_data.get("slit_configuration", None)
        if slit is not None:
            s1w = fileio.Value.from_dict(slit["divergence_opening"]).as_unit("cm")
            s1dist = fileio.Value.from_dict(slit["divergence_distance"]).as_unit("cm")
            s2w = fileio.Value.from_dict(slit["size_opening"]).as_unit("cm")
            s2dist = fileio.Value.from_dict(slit["size_distance"]).as_unit("cm")
            self.inner_beam = s2w - (s1w - s2w) * s2dist / (s1dist + s2dist)
            self.beam_size = (s1w + s2w) * (s1dist + s2dist) / (s1dist - s2dist) - s1w

    def set_absorption_correction(self):
        """
        Calculate absorption correction based on sample length, thickness and incident angle.
        """
        mdata: fileio.Orso = self.sup.info
        lamda = mdata.data_source.measurement.instrument_settings.wavelength.magnitude
        theta = arcsin(self.Q * lamda / (4.0 * pi))
        theta1 = arctan(self.sample_thickness / self.sample_length * 2)
        x = where(theta < theta1, self.sample_length / cos(theta), 2 * self.sample_thickness / sin(theta))
        corr = exp(-CONFIG.MU * lamda * x)
        self.cabs = corr

    def set_footprint_correction(self):
        """
        Calculate footprint corrected based on a homogenous beam that is collimated
        with two slits (as for most neutron instruments).

        ==> The center of the beam has constant intensity, in the outside the intensity
        decreases linearly until.
        """
        mdata: fileio.Orso = self.sup.info
        lamda = mdata.data_source.measurement.instrument_settings.wavelength.magnitude
        theta = arcsin(self.Q * lamda / (4.0 * pi))

        theta2 = arcsin(self.inner_beam / self.sample_length)
        theta3 = arcsin(self.beam_size / self.sample_length)

        full_beam = self.inner_beam + (self.beam_size - self.inner_beam) / 2.0
        scale_outer = (self.beam_size - self.inner_beam) / 2.0 / full_beam

        corr = where(
            theta < theta3,
            where(
                theta < theta2,
                (1.0 - scale_outer) * theta / theta2,
                (1.0 - scale_outer) + (1.0 - (theta - theta3) ** 2 / (theta3 - theta2) ** 2) * scale_outer,
            ),
            1.0,
        )
        self.cfp = corr

    def set_polarization_correction(self):
        Pup = self.sup.info.data_source.measurement.instrument_settings.user_data["incident_polarization"]
        Pdown = self.sdown.info.data_source.measurement.instrument_settings.user_data["incident_polarization"]
        # matrix for generating meaured intensities from reflectivities
        M = array([[self.I0up * Pup, self.I0down * (1 - Pup)], [self.I0up * (1 - Pdown), self.I0down * Pdown]])
        # invert the matrix to back-transform to reflectivities
        self.Pm = inv(M)

    def get_results(self) -> ResultParameters:
        Pup = self.sup.info.data_source.measurement.instrument_settings.user_data["incident_polarization"]
        Pdown = self.sdown.info.data_source.measurement.instrument_settings.user_data["incident_polarization"]
        ref_in_spec = (
            self.alpha < CONFIG.alpha_max and self.R_m_ref > CONFIG.R_ref and self.R_div_max_exp < CONFIG.R_div_max
        )
        pol_in_spec = self.Pmin >= CONFIG.P_min
        if ref_in_spec and pol_in_spec:
            evaluation = "Accepted"
        else:
            evaluation = query_eval_status()
        return ResultParameters(
            run_id=self.sup.info.data_source.sample.name,
            alpha=self.alpha,
            scale=self.scale,
            beam_size=self.beam_size,
            sample_length=self.sample_length,
            sample_thickness=self.sample_thickness,
            R_m_ref=self.R_m_ref,
            R_div_max=self.R_div_max_exp,
            Pmin=self.Pmin,
            Pavg=self.Pavg,
            Pup=Pup,
            Pdown=Pdown,
            ref_in_spec=ref_in_spec,
            pol_in_spec=pol_in_spec,
            evaluation=evaluation,
        )

    def get_plot_data(self) -> PlotData:
        mdata: fileio.Orso = self.sup.info
        return PlotData(
            Q=self.Q,
            Iup=self.Iup / self.I0up,
            dIup=self.dIup / self.I0up,
            Idown=self.Idown / self.I0down,
            dIdown=self.dIdown / self.I0down,
            cabs=self.cabs,
            cfp=self.cfp,
            P=self.P,
            Pm=self.Pm,
            Rup=self.Rup,
            Rdown=self.Rdown,
            axes_labels=dict(
                ref_x=f"{mdata.columns[0].name} [{mdata.columns[0].unit or 1}]",
                ref_y=f"{mdata.columns[1].name} [{mdata.columns[1].unit or 1}]",
                pol_y="Polarization = (Rup-Rdown)/(Rup+Rdown) [%]",
            ),
            meta=MeasurementMeta(
                instrument=mdata.data_source.experiment.instrument,
                start_time=mdata.data_source.experiment.start_date.isoformat(),
                owner=mdata.data_source.owner.name,
            ),
        )


class ReportGenerator:

    def __init__(self, plot_data: PlotData, result: ResultParameters):
        self.result = result
        self.plot_data = plot_data

        self.setup_page()
        self.draw_evaluation()
        self.plot_reflectivity()
        self.plot_corrected_reflectivity()
        self.plot_polarization()
        self.draw_parameters()

        plt.tight_layout(w_pad=0.5)
        plt.savefig(f"NOB_reflectivity_report_{result.run_id}.pdf", dpi=150)
        plt.show()

    def setup_page(self):
        # configure global plot settings
        self.fig = plt.figure(figsize=(210 * MM2INCHES, 297 * MM2INCHES), constrained_layout=False)
        self.fig.suptitle(
            f"Reflectivity FAT report of MAGIC analyzer sputter run NOB_{self.result.run_id}",
            fontsize=12,
            fontweight="bold",
        )
        self.gs = self.fig.add_gridspec(
            nrows=6, ncols=4, left=0.08, right=0.92, top=0.94, bottom=0.05, hspace=1.5, wspace=0.8
        )
        plt.rc("font", size=8)  # controls default text sizes
        plt.rc("axes", titlesize=8)  # fontsize of the axes title
        plt.rc("axes", labelsize=6)  # fontsize of the x and y labels
        plt.rc("xtick", labelsize=6)  # fontsize of the tick labels
        plt.rc("ytick", labelsize=6)  # fontsize of the tick labels
        plt.rc("legend", fontsize=6)  # legend fontsize
        plt.rc("figure", titlesize=10)  # fontsize of the figure title

    def draw_evaluation(self):
        ax = self.fig.add_subplot(self.gs[0, :])
        ax.axis("off")
        ax.text(20, 0, "Measurement Report:", horizontalalignment="center", fontdict={"fontweight": "bold"})
        ax.text(0, -20, "Report Revision:")
        ax.text(0, -35, "Instrument:")
        ax.text(0, -50, "Date Measured:")
        ax.text(0, -65, "Operator:")
        ax.text(20, -20, REPORT_REVISION)
        ax.text(20, -35, self.plot_data.meta.instrument)
        ax.text(20, -50, self.plot_data.meta.start_time)
        ax.text(20, -65, self.plot_data.meta.owner)

        ax.text(65, 0, "Analysis Results:", horizontalalignment="center", fontdict={"fontweight": "bold"})
        ax.text(50, -20, "Reflectivity:")
        ax.text(50, -50, "Polarization:")

        ax.text(65, -20, f"alpha = {self.result.alpha:.4f} ; R(m={CONFIG.M_ref:.1f}) = {self.result.R_m_ref*100:.1f}%")
        ax.text(
            65, -35, f"maximum dip = {self.result.R_div_max*100:.1f}% -> " f"{CONFIG.E_LABEL[self.result.ref_in_spec]}"
        )
        ax.text(
            65,
            -50,
            f"min = {self.result.Pmin*100:.1f}% ; avg = {self.result.Pavg*100:.1f}% -> "
            f"{CONFIG.E_LABEL[self.result.pol_in_spec]}",
        )

        ax.set_xlim(0, 110)
        ax.set_ylim(-80, -2)

        evaluation = self.result.evaluation
        if evaluation.startswith("Accepted"):
            eval_coor = "#00aa00"
        elif evaluation.startswith("Rejected"):
            eval_coor = "#aa0000"
            # fname = fname[:-4] + "_rejected" + fname[-4:]
        else:
            eval_coor = "#aaaa00"
            # fname = fname[:-4] + f"_{res.lower()}" + fname[-4:]

        if self.result.ref_in_spec and self.result.pol_in_spec:
            ax.text(
                60,
                -80,
                evaluation + " - In Specification".upper(),
                fontweight="bold",
                color="#ffffff",
                backgroundcolor=eval_coor,
            )
        else:
            ax.text(
                60,
                -80,
                evaluation + " - Out of Specification".upper(),
                fontweight="bold",
                color="#ffffff",
                backgroundcolor=eval_coor,
            )

        ax.text(
            55,
            -130,
            "Data analysis of reflectivity and polarization",
            fontsize=10,
            ha="center",
            fontweight="bold",
        )

    def draw_parameters(self):
        ax = self.fig.add_subplot(self.gs[5, :])
        ax.axis("off")
        ax.text(50, 0, "Parameters used for analysis:", horizontalalignment="center", fontdict={"fontweight": "bold"})
        ax.text(0, -30, "Polarization correction:")
        ax.text(
            0, -50, r"$P^{\uparrow}=%.3f ~~~~ P^{\downarrow}=%.3f  \Rightarrow$" % (self.result.Pup, self.result.Pdown)
        )
        ax.text(
            0,
            -65,
            r"$R^{\uparrow} = %.3f \cdot I^{\uparrow}  %.3f \cdot I^{\downarrow}$"
            % (self.plot_data.Pm[0, 0], self.plot_data.Pm[0, 1]),
        )
        ax.text(
            0,
            -80,
            r"$R^{\downarrow} = %.3f \cdot I^{\uparrow} + %.3f \cdot I^{\downarrow}$"
            % (self.plot_data.Pm[1, 0], self.plot_data.Pm[1, 1]),
        )

        ax.text(40, -30, "Footprint correction:")
        ax.text(40, -50, f"Beam size: {self.result.beam_size*10.:.2f} mm")
        ax.text(40, -65, f"Sample length: {self.result.sample_length*10.:.1f} mm")

        ax.text(80, -30, "Absorption correction:")
        ax.text(80, -50, f"Sample thickness: {self.result.sample_thickness*10.:.1f} mm")
        ax.text(80, -65, f"Sample length: {self.result.sample_length*10.:.1f} mm")

        ax.set_xlim(0, 110)
        ax.set_ylim(-80, -2)

    def plot_reflectivity(self):
        ax = self.fig.add_subplot(self.gs[1:3, 0:4])
        ax.set_title("Measured Reflectivity")
        plt.errorbar(
            self.plot_data.Q,
            self.plot_data.Iup,
            yerr=self.plot_data.dIup,
            label="spin-up $\\leftarrow$",
        )
        plt.errorbar(
            self.plot_data.Q,
            self.plot_data.Idown,
            yerr=self.plot_data.dIdown,
            label="spin-down $\\leftarrow$",
        )
        plt.yscale("log")

        plt.xlabel(self.plot_data.axes_labels["ref_x"])
        plt.ylabel(self.plot_data.axes_labels["ref_y"])
        plt.xlim(0.0, self.plot_data.Q.max() + 0.005)
        plt.ylim(1e-3, 2.0)

        axl = plt.gca()
        axr = plt.twinx()
        plt.plot(self.plot_data.Q, self.plot_data.cabs, color="#888888", label="absorption effect $\\rightarrow$")
        plt.plot(self.plot_data.Q, self.plot_data.cfp, color="#cccccc", label="footprint effect $\\rightarrow$")

        lines, labels = axl.get_legend_handles_labels()
        lines2, labels2 = axr.get_legend_handles_labels()
        plt.legend(lines + lines2, labels + labels2, loc="lower center")
        plt.ylim(0, 1.5)

        axr.twiny()
        plt.xlabel("m-value")
        plt.ylabel("Intensity Modification")
        plt.xlim(0.0, (CONFIG.M_max / Q2M + 0.005) * Q2M)

    def plot_corrected_reflectivity(self):
        ax = self.fig.add_subplot(self.gs[3:5, 0:2])
        ax.set_title("Corrected Reflectivity")

        corr = self.plot_data.cabs * self.plot_data.cfp

        ref = DataAnalyzer.ideal_reflectivity(self.plot_data.Q * Q2M, 1.0, CONFIG.Alpha_spec)
        calc = DataAnalyzer.ideal_reflectivity(self.plot_data.Q * Q2M, self.result.scale, self.result.alpha)

        if ((self.plot_data.Rup / corr) < ref).any():
            # color the area where reflectivity is below nominal curve
            plt.fill_between(
                self.plot_data.Q,
                self.plot_data.Rup / corr,
                ref,
                where=((self.plot_data.Rup / corr) < ref),
                color="#ff0000",
                alpha=0.3,
            )

        plt.plot(self.plot_data.Q, self.plot_data.Rup / corr, label="spin-up")
        plt.plot(self.plot_data.Q, self.plot_data.Rdown / corr, label="spin-down")
        plt.plot([-0.002, 3.5 / Q2M], [self.result.R_m_ref, self.result.R_m_ref], color="#aaaaaa", linestyle="--")
        plt.plot(self.plot_data.Q, calc, color="#aaaaaa", label="R(m,alpha)")
        plt.plot(self.plot_data.Q, ref * 0.9, color="#ffdddd", linestyle="--")
        plt.plot(self.plot_data.Q, ref, color="#aa0000", label="specification")
        plt.plot([CONFIG.M_ref / Q2M], [CONFIG.R_ref], "o", color="#aa0000")

        plt.text(
            3.5 / Q2M,
            self.result.R_m_ref,
            f"{self.result.R_m_ref*100:.1f} %",
            color="#aaaaaa",
            verticalalignment="center",
        )
        plt.text(1.5 / Q2M, 1.05, f"$\\alpha$-experimental = {self.result.alpha:.3f}", color="#aaaaaa")
        plt.text(1.0 / Q2M, 0.65, f"$\\alpha$-specification = {CONFIG.Alpha_spec:.3f}", color="#aa0000")

        plt.legend(loc="lower center")
        plt.xlabel(self.plot_data.axes_labels["ref_x"])
        plt.ylabel(self.plot_data.axes_labels["ref_y"])
        plt.xlim(0.0, 0.09)

        ax.twiny()
        plt.xlabel("m-value")
        plt.xlim(0.0, 0.09 * Q2M)
        plt.ylim(0.0, 1.25)

    def plot_polarization(self):
        ax = self.fig.add_subplot(self.gs[3:5, 2:4])
        ax.set_title("Calculated Polarization")

        P = 100 * self.plot_data.P
        ref = where(
            (self.plot_data.Q >= CONFIG.Q_Pstart) & (self.plot_data.Q <= CONFIG.Q_Pend), 100 * CONFIG.P_min, nan
        )
        plt.errorbar(self.plot_data.Q, P, color="C2", label="polarization")

        if (P < ref).any():
            plt.fill_between(self.plot_data.Q, P, ref, where=(P < ref), color="#ff0000", alpha=0.3)

        plt.plot(self.plot_data.Q, ref, color="#aa0000", label="specification")
        plt.xlabel(self.plot_data.axes_labels["ref_x"])
        plt.ylabel(self.plot_data.axes_labels["pol_y"])
        plt.legend()
        plt.xlim(0.0, 0.085)
        plt.ylim(0, 105)

        ax.twiny()
        plt.xlabel("m-value")
        plt.xlim(0.0, 0.085 * Q2M)


def query_eval_status():
    if not INTERFACE_GUI:
        options = ""
        for i, option in enumerate(BAD_RESULT_OPTIONS):
            options += f"{i+1}: {option}\n"
        res = input("Out of specification, how do you want to label it?\n\n" + options)
        try:
            return BAD_RESULT_OPTIONS[int(res) - 1]
        except (ValueError, IndexError):
            return BAD_RESULT_OPTIONS[0]
    else:
        tk_root = Tk()
        tk_root.title("MAGIC Reflectivity Analysis - label result")
        tk_root.tk.eval(f"tk::PlaceWindow {tk_root._w} center")
        tk_root.geometry("400x200")

        v = IntVar()
        v.set(0)  # initializing the choice, i.e. Python

        Label(
            tk_root,
            text="""Out of specification, how do you want to label it
        
        Close this window after choice is done.
        """,
            justify=LEFT,
            padx=20,
        ).pack()

        for i, option in enumerate(BAD_RESULT_OPTIONS):
            Radiobutton(tk_root, text=option, padx=20, variable=v, value=i).pack(anchor=W)
        tk_root.mainloop()
        return BAD_RESULT_OPTIONS[v.get()]


def get_input_file():
    if len(sys.argv) > 1:
        return sys.argv[1]
    if INTERFACE_GUI:
        tk_root = Tk()
        tk_root.title("MAGIC Reflectivity Analysis")
        tk_root.tk.eval(f"tk::PlaceWindow {tk_root._w} center")
        # tk_root.withdraw()
        res = filedialog.askopenfilename(
            title="MAGIC Reflectivity Analysis - select datafile...",
            filetypes=[
                ("ORSO Text", "*.ort"),
            ],
        )
        tk_root.destroy()
        return res
    else:
        return input("Please provide file name: ")


def main():
    file_name = get_input_file()
    sup, sdown = read_data(file_name)
    export_path = os.path.dirname(os.path.abspath(file_name))
    os.chdir(export_path)

    analyzer = DataAnalyzer(sup, sdown)
    result = analyzer.get_results()
    plot_data = analyzer.get_plot_data()

    ReportGenerator(plot_data, result)


if __name__ == "__main__":
    main()
